# coding=utf-8
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import re

from ppadb.client import Client as AdbClient


class Helper:
    def __init__(self, pattern):
        self.pattern = pattern

    def log_output(self, connection):
        while True:
            data = connection.read(1024)
            if not data:
                break

            data_lines = data.split(b'\n')
            filtered_by_pattern = [x for x in data_lines if re.search(self.pattern, x)]

            if len(filtered_by_pattern) > 0:
                filtered_string = b'\n'.join(filtered_by_pattern)
                print(filtered_string.decode('utf-8'))

        connection.close()


def adb(mode, target):
    # Default is "127.0.0.1" and 5037
    client = AdbClient(host="127.0.0.1", port=5037)

    if mode == 'backup':
        print(client.version())

    if mode == 'find':
        device = client.device('711KPVH0604767')

        helper = Helper(target)
        device.shell("find ./ 2>&1 | grep -v 'Permission denied'", handler=helper.log_output)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # adb('backup')
    adb('find', b'^.*\.(apk)$')

