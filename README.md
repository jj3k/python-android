# Python Android

This project is an implementation of [pure-python-adb](https://github.com/Swind/pure-python-adb)

It will eventually automate backup and restore APKs, photos, contacts, SQLite DBs, etc.